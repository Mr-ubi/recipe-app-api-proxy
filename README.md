# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage
No usage right now

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` -  Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port to the app to forward requests to (default: `9000`)
